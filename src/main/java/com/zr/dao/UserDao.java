package com.zr.dao;

import net.sf.json.JSONArray;

public interface UserDao<T> extends BaseDao<T>{
	
	/**
	 * 获取指定parentid的全部功能的json数据格式
	 * @param parentid
	 * @return
	 */
	public JSONArray getMethodById(int parentid,int rid);
	
	/**
	 * 将所有功能变为json格式
	 * @param parentid
	 * @return
	 */
	public JSONArray getAllMethodsById(int parentid,int rid);
}
