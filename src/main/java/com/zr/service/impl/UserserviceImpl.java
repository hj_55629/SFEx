package com.zr.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zr.dao.BaseDao;
import com.zr.dao.UserDao;
import com.zr.model.User;
import com.zr.service.UserService;

import net.sf.json.JSONArray;

/**
 * 
 * @author wxz
 * 2016年6月19日下午9:33:50
 */
@Service("userService")
public class UserserviceImpl implements UserService{
//	属性名必须和BaseDao里规定的一样
	@Resource
	private BaseDao<User> baseDao;
	@Resource
	private UserDao<User> userDao;

	@Override
	public User findUserByUserNameAndPsw(User user) {
		String hql="from User u where u.uname=? and u.password=?";
		return baseDao.get(hql,new Object[]{user.getUname(),user.getPassword()});
	}

	@Override
	public JSONArray getAllMethodsById(int parentid, int rid) {
		return userDao.getAllMethodsById(parentid, rid);
	}

	
}
