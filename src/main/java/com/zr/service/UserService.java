package com.zr.service;

import com.zr.model.User;
/**
 * 
 * @author wxz
 * 2016年6月19日下午9:31:53
 */

import net.sf.json.JSONArray;
public interface UserService {

	/**
	 * 查询是否有该用户
	 * @param user
	 * @return
	 */
	public User findUserByUserNameAndPsw(User user);
	
	/**
	 * 获取功能列表
	 * @param parentid
	 * @param rid
	 * @return
	 */
	public JSONArray getAllMethodsById(int parentid,int rid);
}
