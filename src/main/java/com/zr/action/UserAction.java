package com.zr.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.zr.model.User;
import com.zr.service.UserService;

/**
 * 
 * @author wxz
 * 2016年6月19日下午9:40:06
 */
@Controller
@Scope(value="prototype")
public class UserAction extends ActionSupport implements ServletResponseAware,ServletRequestAware{
	
	@Resource
	private UserService userService;
	private HttpServletResponse resp;
	private HttpServletRequest req;
//	-------------------------------------------
	private User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
//	------------------------------------------
	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.req=request;
	}
	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.resp=response;
	}
//	--------------------------------------------
//	登录验证
	public String login(){
//		把当前登录人员存到session中
		ActionContext actionContext=ActionContext.getContext();
		Map<String, Object> session=actionContext.getSession();
		User currentuser = userService.findUserByUserNameAndPsw(user);
		session.put("currentuser", currentuser);
		if(null==currentuser){
			return ERROR;
		}
			return SUCCESS;
	}
//	功能加载
	public void getAllMethods() throws IOException{
//		获取登录人员
		ActionContext actionContext=ActionContext.getContext();
		Map<String, Object> session=actionContext.getSession();
		User currentuser=(User) session.get("currentuser");
		int i=currentuser.getRole().getRid();
		System.out.println("i:"+i);
		String str=userService.getAllMethodsById(1, i).toString();
		resp.setCharacterEncoding("utf8");
		PrintWriter pw=resp.getWriter();
		pw.write(str);
	}

}