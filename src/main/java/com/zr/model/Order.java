package com.zr.model;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="orderorder")
public class Order {
	
	private int oid;
	private String startplace;
	private Date spdate;
	private DistributionCenter distributionCenter;
	private String otype;
	private Set<OrderState> os;
	private String note;
	private String aname;
	private String aphone;
	private String aphone2;
	private String aaddress;
	private String bname;
	private String bphone;
	private String bphone2;
	private String baddress;
	private DistributionCenter distributionCenter2;
	private double cweight;
	private double cvolume;
	private String citemname;
	private int cnumber;
	private Classes classes;
	private double bjmoney;
	private double cost;
	private double totalcost;
	private String payname;
	private String payaddress;
	private Date senddate;
	private Car car;
	
	@Id
	@GeneratedValue
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public String getStartplace() {
		return startplace;
	}
	public void setStartplace(String startplace) {
		this.startplace = startplace;
	}
	public Date getSpdate() {
		return spdate;
	}
	public void setSpdate(Date spdate) {
		this.spdate = spdate;
	}
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="dcid",unique=true)
	public DistributionCenter getDistributionCenter() {
		return distributionCenter;
	}
	public void setDistributionCenter(DistributionCenter distributionCenter) {
		this.distributionCenter = distributionCenter;
	}
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="dcid2",unique=true)
	public DistributionCenter getDistributionCenter2() {
		return distributionCenter2;
	}
	public void setDistributionCenter2(DistributionCenter distributionCenter2) {
		this.distributionCenter2 = distributionCenter2;
	}
	public String getOtype() {
		return otype;
	}
	public void setOtype(String otype) {
		this.otype = otype;
	}
	
	@OneToMany(mappedBy="order",cascade=CascadeType.ALL)
	public Set<OrderState> getOs() {
		return os;
	}
	public void setOs(Set<OrderState> os) {
		this.os = os;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getAname() {
		return aname;
	}
	public void setAname(String aname) {
		this.aname = aname;
	}
	public String getAphone() {
		return aphone;
	}
	public void setAphone(String aphone) {
		this.aphone = aphone;
	}
	public String getAphone2() {
		return aphone2;
	}
	public void setAphone2(String aphone2) {
		this.aphone2 = aphone2;
	}
	public String getAaddress() {
		return aaddress;
	}
	public void setAaddress(String aaddress) {
		this.aaddress = aaddress;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getBphone() {
		return bphone;
	}
	public void setBphone(String bphone) {
		this.bphone = bphone;
	}
	public String getBphone2() {
		return bphone2;
	}
	public void setBphone2(String bphone2) {
		this.bphone2 = bphone2;
	}
	public String getBaddress() {
		return baddress;
	}
	public void setBaddress(String baddress) {
		this.baddress = baddress;
	}
	public double getCweight() {
		return cweight;
	}
	public void setCweight(double cweight) {
		this.cweight = cweight;
	}
	public double getCvolume() {
		return cvolume;
	}
	public void setCvolume(double cvolume) {
		this.cvolume = cvolume;
	}
	public String getCitemname() {
		return citemname;
	}
	public void setCitemname(String citemname) {
		this.citemname = citemname;
	}
	public int getCnumber() {
		return cnumber;
	}
	public void setCnumber(int cnumber) {
		this.cnumber = cnumber;
	}
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="clid")
	public Classes getClasses() {
		return classes;
	}
	public void setClasses(Classes classes) {
		this.classes = classes;
	}
	public double getBjmoney() {
		return bjmoney;
	}
	public void setBjmoney(double bjmoney) {
		this.bjmoney = bjmoney;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getTotalcost() {
		return totalcost;
	}
	public void setTotalcost(double totalcost) {
		this.totalcost = totalcost;
	}
	public String getPayname() {
		return payname;
	}
	public void setPayname(String payname) {
		this.payname = payname;
	}
	public String getPayaddress() {
		return payaddress;
	}
	public void setPayaddress(String payaddress) {
		this.payaddress = payaddress;
	}
	public Date getSenddate() {
		return senddate;
	}
	public void setSenddate(Date senddate) {
		this.senddate = senddate;
	}
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="cid")
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	
	
	
}
