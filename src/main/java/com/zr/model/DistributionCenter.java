package com.zr.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="distributionCenter")
public class DistributionCenter {
	private int did;
	private String dname;
	private String dps_dll;
	private double dprice;
	private String dcode;
	private String dmgname;
	private String dmgcommand;
	private int upid;
	private int downid;
	private String dzxorzx;
	private String dzxsite;
	private Set<DistributionScope> ds;
	
	@Id
	@GeneratedValue
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getDps_dll() {
		return dps_dll;
	}
	public void setDps_dll(String dps_dll) {
		this.dps_dll = dps_dll;
	}
	public double getDprice() {
		return dprice;
	}
	public void setDprice(double dprice) {
		this.dprice = dprice;
	}
	public String getDcode() {
		return dcode;
	}
	public void setDcode(String dcode) {
		this.dcode = dcode;
	}
	public String getDmgname() {
		return dmgname;
	}
	public void setDmgname(String dmgname) {
		this.dmgname = dmgname;
	}
	public String getDmgcommand() {
		return dmgcommand;
	}
	public void setDmgcommand(String dmgcommand) {
		this.dmgcommand = dmgcommand;
	}
	public int getUpid() {
		return upid;
	}
	public void setUpid(int upid) {
		this.upid = upid;
	}
	public int getDownid() {
		return downid;
	}
	public void setDownid(int downid) {
		this.downid = downid;
	}
	public String getDzxorzx() {
		return dzxorzx;
	}
	public void setDzxorzx(String dzxorzx) {
		this.dzxorzx = dzxorzx;
	}
	public String getDzxsite() {
		return dzxsite;
	}
	public void setDzxsite(String dzxsite) {
		this.dzxsite = dzxsite;
	}
	
	@OneToMany(mappedBy="dc",cascade=CascadeType.ALL)
	public Set<DistributionScope> getDs() {
		return ds;
	}
	public void setDs(Set<DistributionScope> ds) {
		this.ds = ds;
	}
	
	
}
