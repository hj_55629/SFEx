package com.zr.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="car",catalog="zrexpress")
public class Car {
	
	private int cid;
	private String cname;
	private double cload;
	private String cmode;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public double getCload() {
		return cload;
	}
	public void setCload(double cload) {
		this.cload = cload;
	}
	public String getCmode() {
		return cmode;
	}
	public void setCmode(String cmode) {
		this.cmode = cmode;
	}
	
}
