package com.zr.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="orderstate")
public class OrderState {

	private int osid;
	private String osfzd;
	private String location;
	private String nextlocation;
	private Date time;
	private Order order;
	
	@Id
	@GeneratedValue
	public int getOsid() {
		return osid;
	}
	public void setOsid(int osid) {
		this.osid = osid;
	}
	public String getOsfzd() {
		return osfzd;
	}
	public void setOsfzd(String osfzd) {
		this.osfzd = osfzd;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getNextlocation() {
		return nextlocation;
	}
	public void setNextlocation(String nextlocation) {
		this.nextlocation = nextlocation;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	@ManyToOne
	@JoinColumn(name="oid")
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	
}
